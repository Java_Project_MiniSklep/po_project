package model;

import java.util.HashMap;
import java.util.Map;

public class ProductSet {
	static Map<Integer, AGD> AGDmap = new HashMap<Integer, AGD>();
	static Map<Integer, RTV> RTVmap = new HashMap<Integer, RTV>();
	public ProductSet(){}
	
	public static void insertAGD(int a, AGD b){
		AGDmap.put(a, b);
	}
	public static void insertRTV(int a, RTV b){
		RTVmap.put(a, b);
	}
}
