package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TV extends RTV implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2922538203435275355L;
	static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "OLED");
		types.put(2,  "LED");
	}
	
	static final Map<Integer, String> HDtypes = new HashMap<Integer, String>();
	static{
		types.put(1, "UHD");
		types.put(2,  "Full HD");
		types.put(3,  "HD Ready");
	}
	
	private int garantee;
	private boolean has3d;
	private String type;
	private String HDtype;
	private double size;
	private boolean hasWiFi;
	private boolean hasSmartTV;
	TV(String seller, String brand, double price, String productName, String energeticClass, String color,
			boolean hasGuarantee, boolean has3d, int type, int HDtype, double size, boolean hasWiFi, boolean hasSmartTV) {
		super(seller, brand, price, productName, energeticClass, color, hasGuarantee);
		this.has3d = has3d;
		this.type = types.get(type);
		this.HDtype = HDtypes.get(HDtype);
		this.size = size;
		this.hasWiFi = hasWiFi;
		this.hasSmartTV = hasSmartTV;
		
	}
	public boolean withSmartTV(){
		return hasSmartTV;
	}
	public boolean with3D(){
		return has3d;
	}
	public boolean getHasGarantee(){
		return super.getHasGarantee();
	}
	public boolean withWiFi(){
		return this.hasWiFi;
	}
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}
	public String getType(){
		return this.type;
	}
	public String getHDType(){
		return this.HDtype;
	}
	public double getSize(){
		return this.size;
	}

}
