package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Client extends Person implements Serializable {

		/**
		 * 
		 */
		Set<RTV> rtv;
		Set<AGD> agd;
		private static final long serialVersionUID = -2673500453592415595L;
		
		public Client(String firstName, String secondName) {
			super(firstName, secondName);
			rtv = new HashSet<RTV>();
			agd = new HashSet<AGD>();
			
		}
}
