package model;

import java.util.HashMap;
import java.util.Map;

public class SellerSet {
	static Map<Integer, Seller> map = new HashMap<Integer, Seller>();
	public SellerSet(){}
	
	public static void insert(int a, Seller b){
		map.put(a, b);
	}
	public static Seller get(int id){
		return map.get(id);
	}
}
