package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Seller extends Person implements Serializable {

	/**
	 * 
	 */
	double profit;
	Set<RTV> rtv;
	Set<AGD> agd;
	private static final long serialVersionUID = -2673500453592415595L;
	
	public Seller(String firstName, String secondName) {
		super(firstName, secondName);
		rtv = new HashSet<RTV>();
		agd = new HashSet<AGD>();
		profit = -1;
		
	}
	public double getProfit(){
		return profit;
	}

	public void giveRTV(RTV produkt) {
	}

	public void giveAGD(AGD produkt) {
	}

	public void removeRTV(RTV produkt) {
	}

	public void removeAGD(AGD produkt) {
	}
}