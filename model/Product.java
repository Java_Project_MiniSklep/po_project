package model;

public interface Product {
	
	double getPrice();
	
	String getSeller();
	
	String getBrand();
}
