package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Dryers extends AGD implements Serializable{
	//suszarki
	/**
	 * 
	 */
	private static final long serialVersionUID = 4399535730165277636L;
	
	static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "wywiewna");
		types.put(2,  "kondensacyjna");
	}
	
	private String type;
	
	private int maxWeightToPutIn;
	private int garantee;
	
	Dryers(Seller seller, String brand, double price, String productName, double depth, double height, double width,
			String energeticClass, String color, boolean hasGuarantee, int maxWeightToPutIn, int typeInt, int number) {
		super(seller, brand, price, productName, depth, height, width, energeticClass, color, hasGuarantee, number);
		
		this.maxWeightToPutIn = maxWeightToPutIn;
		this.type = types.get(typeInt);
	}
	
	public boolean getHasGarantee(){
		return super.getHasGarantee();
	}
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}
	
	public int getMaxWeight(){
		return this.maxWeightToPutIn;
	}
	
	public String getType(){
		return this.type;
	}
	
}
