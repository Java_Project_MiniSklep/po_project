package model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataCheckClass {
	public DataCheckClass(){}
	
	public boolean nameCheck(String s){
		return s.matches("[a-zA-Z]+");
	}
	public boolean loginCheck(String s){
		 Pattern p = Pattern.compile("^[a-zA-Z0-9_-]{3,15}$");  
	     Matcher m = p.matcher(s);  
	     return m.matches();  
	}
	public boolean passwordCheck(String s){
		return s.length() > 6;
	}
	public boolean phoneCheck(String s){
		return s.matches("[0-9]+");
	}
	public boolean e_mailCheck(String s){
		String ePattern = "^[a-zA-Z0-9_-]+@[a-z]+\\.[a-z]+$";
        Pattern p = java.util.regex.Pattern.compile(ePattern);
        Matcher m = p.matcher(s);
        return m.matches();
	}
}
