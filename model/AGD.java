package model;

public class AGD implements Product{
	String productName;
	
	double price;
	
	Seller seller;
	
	String brand;
	String color;
	
	double depth;
	double width;
	double height;
	
	String energeticClass;
	int number_of_items;
	boolean hasGuarantee;
	
	AGD(Seller seller, String brand, double price, String productName, double depth, double height, double width, 
			String energeticClass, String color, boolean hasGuarantee, int number_of_items){
		this.seller = seller;
		this.brand = brand;
		this.price = price;
		this.productName = productName;
		this.color = color;
		this.depth = depth;
		this.height = height;
		this.width = width;
		this.energeticClass = energeticClass;
		this.hasGuarantee = hasGuarantee;
		this.number_of_items = number_of_items;
		
	}
	
	

	int getNumber(){
		return number_of_items;
	}
	void sell(int number){
		number_of_items -= number;
	}
	public String getColor(){
		return color;
	}
	public double[] getSize(){
		return new double[]{depth, width, height};
	}
	public String getEnergeticClass(){
		return energeticClass;
	}
	public boolean getHasGarantee(){
		return hasGuarantee;
	}
	public double getPrice(){
		return price;
	}
	public String getSeller(){
		return seller.getSecondName();
	}
	public String getBrand(){
		return brand;
	}
}
