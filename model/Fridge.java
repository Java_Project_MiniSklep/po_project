package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Fridge extends AGD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "Lodówki");
		types.put(2,  "Lodówko - zamrażarki");
		types.put(3, "Lodówki 'side by side'");
		types.put(4, "Lodówki do zabudowy");
	}
	
	String type;
	private boolean hasFreezer;
	private int garantee; //number of months
	
	Fridge(Seller seller, String brand, double price, String productName, int type, double depth, double height, double width, 
			String energeticClass, String color, boolean hasFreezer, boolean hasGuarantee, int number) {
		super(seller, brand,  price,  productName,  depth,  height,  width, 
				 energeticClass,  color,  hasGuarantee, number);
		this.type = types.get(type);
		this.hasFreezer = hasFreezer;
		
		
	}
	
	
	public String getType(){
		return type;
	}
	
	public boolean getFreezer(){
		return hasFreezer;
	}
	public boolean getHasGarantee(){
		return super.getHasGarantee();
	}
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}

}
