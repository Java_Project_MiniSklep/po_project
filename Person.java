package data;

import java.io.Serializable;

public class Person implements Serializable {
	private static final long serialVersionUID = -6416675483722440724L;
	private String firstName;
	private String lastName;

	public Person(String name, String lastName) {
		super();
		this.firstName = name;
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Person [Name=" + firstName + ", Lastname=" + lastName + "]";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return firstName + " " + lastName;
	}
}
