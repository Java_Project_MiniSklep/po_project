package data;

import java.io.Serializable;
import java.util.Date;

public class Client extends Person implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 118140142952654654L;
	private Date dateOfCreation;

	public Client(String firstName, String lastName) {
		super(firstName, lastName);
		this.dateOfCreation = new Date();
	}

	public Date getDateOfCreation() {
		return (Date) dateOfCreation.clone();
	}

	public void setCreatedOn(Date dateNew) {
		this.dateOfCreation = dateNew;
	}

	public Client() {
		super("Adam", "Kowalski");
		this.dateOfCreation = new Date();
	}
}
