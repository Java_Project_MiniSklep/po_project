package model;

public class AGD implements Product{
	String productName;
	
	double price;
	
	String seller;
	
	String brand;
	String color;
	
	double depth;
	double width;
	double height;
	
	String energeticClass;
	
	boolean hasGuarantee;
	
	AGD(String seller, String brand, double price, String productName, double depth, double height, double width, 
			String energeticClass, String color, boolean hasGuarantee){
		this.seller = seller;
		this.brand = brand;
		this.price = price;
		this.productName = productName;
		this.color = color;
		this.depth = depth;
		this.height = height;
		this.width = width;
		this.energeticClass = energeticClass;
		this.hasGuarantee = hasGuarantee;
		
	}
	
	public String getColor(){
		return color;
	}
	public double[] getSize(){
		return new double[]{depth, width, height};
	}
	public String getEnergeticClass(){
		return energeticClass;
	}
	public boolean getHasGarantee(){
		return hasGuarantee;
	}
	public double getPrice(){
		return price;
	}
	public String getSeller(){
		return seller;
	}
	public String getBrand(){
		return brand;
	}
}
