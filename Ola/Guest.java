package model;

public class Guest {
	String nickname;
	Guest(){
		nickname = "~";
	}
	Guest(String name){
		nickname = name;
	}
	@Override
	public String toString() {
		return "Nickname: " +nickname;
	}
	public String getNickname(){
		return this.nickname;
	}
	public void setNickname(String newNick){
		this.nickname = newNick;
	}
}
