package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class WashingMachines extends AGD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "Sposób załadunku - od góry");
		types.put(2,  "Sposób załadunku - od przodu");
	}
	
	private String type;
	
	private int maxWeightToPutIn;
	private int maxSpeed; //Maksymalna prędkość wirowania, obr/min
	private int garantee;
	
	WashingMachines(String seller, String brand, double price, String productName, double depth, double height,
			double width, String energeticClass, String color, boolean hasGuarantee, int type, int maxWeightToPutIn,
			int maxSpeed) {
		super(seller, brand, price, productName, depth, height, width, energeticClass, color, hasGuarantee);
		this.type = types.get(type);
		this.maxSpeed = maxSpeed;
		this.maxWeightToPutIn = maxWeightToPutIn;
	}
	public boolean getHasGarantee(){
		return super.getHasGarantee();
	}
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}
	
	public int getMaxWeight(){
		return this.maxWeightToPutIn;
	}
	public int getMaxSpeed(){
		return this.maxSpeed;
	}
	public String getType(){
		return this.type;
	}
}
