package panels;

import java.awt.Cursor;
import java.awt.GridBagLayout;


import javax.swing.JButton;
import javax.swing.JFrame;

public class SellerLogIn {
	public void main_function(){
		
		//-----FRAME
        JFrame frame = new JFrame("Sklep");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        //-----ELEMENTS ON FRAME
        
        // seller log in
        JButton logIn = new JButton("Log in");
        logIn.setCursor(new Cursor(Cursor.HAND_CURSOR));        
        JButton signIn = new JButton("Sign up");
        signIn.setCursor(new Cursor(Cursor.HAND_CURSOR));
        
        frame.add(logIn);
        frame.add(signIn);
        
        frame.setVisible(true);
	};
}
