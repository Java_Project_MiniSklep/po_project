package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Earphones extends RTV implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3489009103364596866L;
	static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "douszne");
		types.put(2,  "nauszne");
		types.put(3,  "bezprzwodowe");
		types.put(4,  "przwodowe");
	}
	static final Map<Integer, String> destinationSet = new HashMap<Integer, String>();
	static{
		types.put(1, "uniwersalne");
		types.put(2,  "do telefonow");
		types.put(3,  "iPod/iPhone/iPad");
		types.put(4,  "sportowe");
	}
	private String type; 
	private int garantee;
	private String destination;
	Earphones(String seller, String brand, double price, String productName, String energeticClass, String color,
			boolean hasGuarantee, int type1, int type2, int destination) {
		super(seller, brand, price, productName, energeticClass, color, hasGuarantee);
		this.type = types.get(type1)+types.get(type2);
		this.destination = destinationSet.get(destination); 
		
	}
	
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}
	public String getType(){
		return this.type;
	}
	public String getDestination(){
		return this.destination;
	}

}
