package panels;

import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MainPanel {
	public static void main(String[] args) {
        {
        	//-----FRAME
            JFrame frame = new JFrame("Sklep");
            frame.setSize(500, 500);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLayout(new GridBagLayout());
            
            //-----ELEMENTS ON FRAME
            
            //--guest
            JButton logAsGuest = new JButton( "Log in as guest");         
            logAsGuest.setCursor(new Cursor(Cursor.HAND_CURSOR));
            
            logAsGuest.addActionListener(new ActionListener() {
            	 
				@Override
				public void actionPerformed(ActionEvent arg0) {
					 frame.setVisible(false);
					 
				}
            });
            
            frame.add(logAsGuest);
            
            
            // -- client
            JButton logAsClient = new JButton( "Log in as client");
            logAsClient.setCursor(new Cursor(Cursor.HAND_CURSOR));
            
            logAsClient.addActionListener(new ActionListener() {
           	 
				@Override
				public void actionPerformed(ActionEvent arg0) {
					 frame.setVisible(false);
					 new ClientLogIn().main_function();
				}
            });
            frame.add(logAsClient);
            
            //--seller
            JButton logAsSeller = new JButton( "Log in as seller");
            logAsSeller.setCursor(new Cursor(Cursor.HAND_CURSOR));
            
            logAsClient.addActionListener(new ActionListener() {
              	 
				@Override
				public void actionPerformed(ActionEvent arg0) {
					 frame.setVisible(false);
					 new SellerLogIn().main_function();
				}
            });
            
            frame.add(logAsSeller);
            
            
            frame.setVisible(true);
        };
    }
}
