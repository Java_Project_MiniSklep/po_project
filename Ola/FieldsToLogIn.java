package panels;

import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class FieldsToLogIn {
	public void main_function(){
		//-----FRAME
        JFrame frame = new JFrame("Log in");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        JTextField firstName = new JTextField();
        frame.add(firstName);
        
        JPasswordField password = new JPasswordField();
        frame.add(password);
	} 
}
