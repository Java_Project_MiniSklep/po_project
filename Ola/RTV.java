package model;

public class RTV implements Product{
	String productName;
	double price;
	String seller;
	String brand;
	String color;
	String energeticClass;	
	boolean hasGuarantee;
	
	RTV(String seller, String brand, double price, String productName, String energeticClass, String color, boolean hasGuarantee){
		this.seller = seller;
		this.brand = brand;
		this.price = price;
		this.productName = productName;
		this.energeticClass = energeticClass;
		this.hasGuarantee = hasGuarantee;
		this.color = color;
	}
	

	public String getSeller(){
		return seller;
	}
	public String getBrand(){
		return brand;
	}
	public String getColor(){
		return color;
	}
	public boolean getHasGarantee(){
		return hasGuarantee;
	}
	public double getPrice(){
		return price;
	}
	
}
