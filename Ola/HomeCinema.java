package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HomeCinema extends RTV implements Serializable{
	private static final long serialVersionUID = 2922538203435275355L;
	static final Map<Integer, String> types = new HashMap<Integer, String>();
	static{
		types.put(1, "amplituner i odtwarzacz Blu-ray w jednej obudowie oraz kolumny");
		types.put(2,  "amplituner i odtwarzacz Blu-ray w osobnych obudowach oraz kolumny");
		types.put(3,  "amplituner i kolumny");
	}
	
	private String type;
	private int garantee;
	private boolean hasWiFi;
	private boolean has3d;
	private boolean withAirPlay;
	private int maxVolume;
	
	HomeCinema(String seller, String brand, double price, String productName, String energeticClass, String color,
			boolean hasGuarantee, int type, boolean hasWiFi,  boolean has3d, boolean withAirPlay, int maxVolume) {
		super(seller, brand, price, productName, energeticClass, color, hasGuarantee);
		this.type = types.get(type);
		this.hasWiFi = hasWiFi;
		this.has3d = has3d;
		this.withAirPlay = withAirPlay;
		this.maxVolume = maxVolume;
		// TODO Auto-generated constructor stub
	}
	public boolean with3D(){
		return has3d;
	}
	public int maxVolume(){
		return this.maxVolume;
	}
	public boolean hasAirPlay(){
		return withAirPlay;
	}
	public boolean getHasGarantee(){
		return super.getHasGarantee();
	}
	public boolean withWiFi(){
		return this.hasWiFi;
	}
	public void setGarantee(int month){
		this.garantee = month;
	}
	public int getGarantee(){
		return this.garantee;
	}
	public String getType(){
		return this.type;
	}

}
