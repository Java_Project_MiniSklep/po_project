Programowanie obiektowe
Projekt "Mini Sklep"

Olha Hnatiuk, Lev Deliatynskyi, Michal Kmak

Program obsługujący sklep RTV i AGD. Wzkorzystujący bazę danych SQlite i Swing GUI dla przechowywania, wyświetlania i 
powiązania ze sobą informacji o klientach, sprzedawcach oraz produktach. 

Struktura aplikacji
I. Pakiet domyślny

MainPanel
	Panel, który odpowiada za wybor logowania. Odsyła się do takich klas z pakietu panels jak LogIn, SignUp i UserPanel


II. Pakiet model
Pakiet przesnaczony do zarządzania danymi aplikacji.

Dryers, Fridge, WashingMachines - 
	przeznaczeniem tych klas jest reprezentowanie pewnego produktu dostępnego w sklepie. 
	Każda taka klasa dziedziczy po AGD
Earphones, HomeCinema, TV - 
	Przeznaczeniem tych klas jest reprezentowanie pewnego produktu dostępnego w sklepie. 
	Każda taka klasa dziedziczy po RTV

RTV, AGD - 
	Przeznaczeniem tych klas jest reprezentowanie właściwości wspólnych dla wszystkich
	produktów dostępnych w sklepie. Rozszerzają Product.
Product - 
	interfejs, ktory reprezentuje product.

Seller, Client, Guest -
	reprezentacja osób, które mają różne możliwości. Rozszerzają Person.

Person -
	ogólna reprezentacja osoby.

ClientSet, ProductSet, SellerSet - 
	reprezentacja zbiorów tych obiektów przy użuciu java.util.Set;

DataCheckClass -
	sprawdza poprawność wpisywanych przez użytkownika informacji (np. hasło, mail etc).
SqlConnector -
	klasa, która łączy projekt z bazą danych SQlite;


III. Paket panels

LogIn, SignUp, UserPanel, SellerLogIn, UserLogIn - 
	Panele reprezentujące różne sposoby logowania do sklepu.

AGDAddPanel, RTVAddPanel -
	Panele odpowiedzialne za dodawanie nowych produktów.

SellerPanel, UserPanel, UnsignedUserTable -
	Panele które wyświetlają dane sklep zgodnie z trybem logowania.

SellersSoldObjects, SellersObjectPanel -
	Panele które wyświetlają dane o produktach tego sprzedawcy.


