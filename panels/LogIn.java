package panels;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class LogIn extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2165736671710692088L;
	private JPanel contentPane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn frame = new LogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogIn() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Log in as user");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				java.awt.Window win[] = java.awt.Window.getWindows(); 
				for(int i=0;i<win.length;i++){ 
					win[i].dispose(); 
			} 
			UserLogIn log = new UserLogIn();
			log.setVisible(true);
			}
		});
		btnNewButton.setBounds(52, 158, 148, 62);
		contentPane.add(btnNewButton);
		
		JButton btnLogInAs = new JButton("Log in as seller");
		btnLogInAs.setBounds(237, 158, 148, 62);
		contentPane.add(btnLogInAs);
		btnLogInAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				java.awt.Window win[] = java.awt.Window.getWindows(); 
					for(int i=0;i<win.length;i++){ 
						win[i].dispose(); 
				} 
				SellerLogIn log = new SellerLogIn();
				log.setVisible(true);
			}
		});
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(62, 11, 128, 128);
		contentPane.add(lblNewLabel);
		Image img = new ImageIcon(this.getClass().getResource("/User.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		
		JLabel label = new JLabel("");
		label.setBounds(246, 11, 128, 128);
		contentPane.add(label);
		Image img1 = new ImageIcon(this.getClass().getResource("/Seller.png")).getImage();
		label.setIcon(new ImageIcon(img1));
		
	}

}
