package panels;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.EmptyBorder;

import model.SqlConnector;

import java.sql.*;
import javax.swing.*;
public class UserLogIn extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1545620789299788260L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserLogIn frame = new UserLogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	Connection connection = null;

	/**
	 * Create the frame.
	 */
	public UserLogIn() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(287, 56, 130, 35);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblName = new JLabel("UserName");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblName.setBounds(186, 54, 91, 35);
		contentPane.add(lblName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(186, 125, 83, 35);
		contentPane.add(lblPassword);
		
		JLabel label = new JLabel("");
		label.setBounds(10, 35, 166, 165);
		contentPane.add(label);
		Image img = new ImageIcon(this.getClass().getResource("/logout-icon.png")).getImage();
		label.setIcon(new ImageIcon(img));
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setHorizontalAlignment(SwingConstants.CENTER);
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnLogin.setBounds(248, 193, 113, 46);
		contentPane.add(btnLogin);
		Image img1 = new ImageIcon(this.getClass().getResource("/Ok.png")).getImage();
		btnLogin.setIcon(new ImageIcon(img1));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String query = "select * from Users where login=? and password=?";
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, textField.getText());
					pst.setString(2, new String(passwordField.getPassword()));
					
					ResultSet rs= pst.executeQuery();
					
					int count = 0;
					while(rs.next()){
						count++;
					}
					if(count==1){
						JOptionPane.showMessageDialog(null, "Username and password is correct");
					}
					else{
						JOptionPane.showMessageDialog(null, "Username or password is not correct");
					}
					rs.close();
					pst.close();
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, e1);
					
				}
			}
		});
		
		passwordField = new JPasswordField();
		passwordField.setBounds(287, 125, 130, 35);
		contentPane.add(passwordField);
		
		
		connection = SqlConnector.dbConnector();
		
	}

}
