package panels;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.SqlConnector;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ScrollPaneConstants;

public class SellersObjectPanel extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6453628902857047738L;
	
	public static String convertToMultiline(String orig)
	{
	    return "<html>" + orig.replaceAll("\n", "<br>");
	}
	Connection connection = null;
	private JPanel contentPane;
	
	int seller_id=-1;
	void setSeller(int id){
		
		seller_id = id;
		changeData();
		
	}
	JLabel  lblData;
	void changeData(){
		if(seller_id!=-1){
		String s="";
		
		try {
			connection = SqlConnector.dbConnector();
			
			String query = "select * from AGD where owner_id = "+Integer.toString(seller_id);
			String query1 = "select * from RTV where seller_id = "+Integer.toString(seller_id);
			
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs= pst.executeQuery();
			while(rs.next()){
				s+=rs.getString("opis")+'\n';
			}
			pst = connection.prepareStatement(query1);
			rs= pst.executeQuery();
			while(rs.next()){
				s+=rs.getString("opis")+'\n';
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		lblData.setText(convertToMultiline(s));
		}
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SellersObjectPanel frame = new SellersObjectPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SellersObjectPanel() {
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 422, 251);
		contentPane.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.WHITE);
		panel.setToolTipText("My products");
		scrollPane.setColumnHeaderView(panel);
		
		JLabel lblMyProducts = new JLabel("My products");
		lblMyProducts.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblMyProducts);
		
		
		String s="";
		
		lblData = new JLabel(s);
		lblData.setBackground(SystemColor.inactiveCaptionBorder);
		lblData.setVerticalAlignment(SwingConstants.TOP);
		scrollPane.setViewportView(lblData);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setRowHeaderView(scrollPane_1);
		
	}
}
