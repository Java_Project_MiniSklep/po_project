package panels;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class SellerPanel {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SellerPanel window = new SellerPanel();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SellerPanel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblProfit = new JLabel("Profit: 0$");
		lblProfit.setBounds(10, 11, 65, 14);
		frame.getContentPane().add(lblProfit);
		
		JButton btnNewButton = new JButton("Sold products");
		btnNewButton.setBounds(74, 7, 174, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("My products");
		btnNewButton_1.setBounds(250, 7, 182, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblAddNewProduct = new JLabel("Add new product", SwingConstants.CENTER);
		lblAddNewProduct.setBounds(0, 49, 442, 14);
		frame.getContentPane().add(lblAddNewProduct);
		
		JLabel lblType = new JLabel("Type");
		lblType.setBounds(10, 74, 46, 14);
		frame.getContentPane().add(lblType);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(94, 70, 70, 22);
		frame.getContentPane().add(comboBox);
	}
}
