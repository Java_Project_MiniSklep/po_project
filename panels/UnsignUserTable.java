package panels;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import model.Client;
import model.SqlConnector;
import net.proteanit.sql.*;

public class UnsignUserTable extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -499567870045726670L;
	
	Client client = null;
	Connection connection= null;
	private JTable table;
	
	public void setData(String MainType, String brand, String color, String itemType,
			String model, double minprice, double maxPrice, boolean withGuarantee){
		try{
			connection = SqlConnector.dbConnector();
			String query="";
			if(MainType=="AGD"){
				query = "select brand, energetic_class, price, color, depth, height, width, has_guarantee, model from " +MainType + " where number_of_items >0" ;
			}
			else{
				query = "select brand, energetic_class, price, color, has_guarantee, model from " +MainType + " where number_of_items >0" ;
			}
			if(brand!="" || color!="" || itemType !="" || model !="" || minprice!=0 || maxPrice!=-1 || withGuarantee){
				
				if(brand!=""){
					
					query += " and brand="+"\"" + brand+"\"";
				}
				if(color!=""){
					
						query += " and color = " + "\"" + color + "\"";
					
				}
				if(itemType!= ""){
					
						query += " and product_name = " + "\""+ itemType + "\"";
					
				}
				if(model != ""){
					query += " and model = " + "\""+ model + "\"";
						
				}
				if(maxPrice >0){
					
					query += " and price < " + maxPrice;
						
				}
				if(minprice > 0){
					query += " and price > " + minprice;
				}
				if(withGuarantee){
					query += " and has_guarantee = " + "\""+ withGuarantee + "\"";
				}
				System.out.println(query);
			}
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs= pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
		}catch(Exception e){
			
		}
		
	}
	
	public void setOwner(Client client){
		this.client = client;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UnsignUserTable frame = new UnsignUserTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public UnsignUserTable() {
		
		setBounds(100, 100, 785, 399);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 757, 350);
		getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
	}
}
