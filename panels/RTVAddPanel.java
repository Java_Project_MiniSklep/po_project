package panels;

import java.sql.Connection;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.DataCheckClass;
import model.ProductSet;
import model.RTV;
import model.Seller;
import model.SqlConnector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

public class RTVAddPanel extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2361402718535830894L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	Connection connection = null;
	Seller seller;
	private int seller_id;
	public void setSeller(int id){
		try{
		connection = SqlConnector.dbConnector();
		String query = "select * from sellers where id = " +id;
		PreparedStatement pst = connection.prepareStatement(query);
		ResultSet rs= pst.executeQuery();
		while(rs.next()){
			seller = new Seller(rs.getString("name"), rs.getString("surname"));
		}
		rs.close();
		pst.close();
		}catch(Exception e){}
		
		seller_id = id;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RTVAddPanel frame = new RTVAddPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RTVAddPanel() {
		
		setBounds(100, 100, 325, 299);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddRtv = new JLabel("Add RTV");
		lblAddRtv.setBounds(10, 11, 297, 14);
		lblAddRtv.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddRtv.setFont(new Font("Tahoma", Font.BOLD, 14));
		contentPane.add(lblAddRtv);
		
		JLabel lblNewLabel = new JLabel("Type");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 36, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Brand");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(10, 61, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblColor.setBounds(10, 86, 46, 14);
		contentPane.add(lblColor);
		
		JLabel lblModelName = new JLabel("Model name");
		lblModelName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblModelName.setBounds(10, 111, 85, 14);
		contentPane.add(lblModelName);
		
		JLabel lblEnergeticClass = new JLabel("Energetic class");
		lblEnergeticClass.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnergeticClass.setBounds(10, 136, 112, 14);
		contentPane.add(lblEnergeticClass);
		
		JLabel lblWithGuarantee = new JLabel("With guarantee");
		lblWithGuarantee.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblWithGuarantee.setBounds(10, 161, 96, 14);
		contentPane.add(lblWithGuarantee);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPrice.setBounds(10, 185, 46, 14);
		contentPane.add(lblPrice);
		
		JLabel lblNumberOfObjects = new JLabel("Number of objects");
		lblNumberOfObjects.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNumberOfObjects.setBounds(10, 210, 117, 14);
		contentPane.add(lblNumberOfObjects);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"TV", "Home Cinema", "Earphines"}));
		comboBox.setBounds(147, 33, 149, 22);
		contentPane.add(comboBox);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"HP", "IBM", "Sumsung", "Panasonic", "Kingston", "IBM", "Lenovo", "LG", "Trend micro", "Sony", "Toshiba", "GoPro"}));
		comboBox_1.setBounds(147, 58, 149, 22);
		contentPane.add(comboBox_1);
		
		textField = new JTextField();
		textField.setBounds(147, 84, 149, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(147, 109, 149, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.setModel(new DefaultComboBoxModel<String>(new String[] {"A+", "A", "B", "C", "D", "E", "F", "G"}));
		comboBox_2.setBounds(147, 133, 149, 22);
		contentPane.add(comboBox_2);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.setBounds(147, 158, 97, 23);
		contentPane.add(checkBox);
		
		textField_2 = new JTextField();
		textField_2.setBounds(147, 183, 149, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(147, 208, 149, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAdd.setBounds(122, 235, 91, 23);
		contentPane.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try{
						if(textField.getText().isEmpty() ||
						   textField_1.getText().isEmpty()	||
						   textField_2.getText().isEmpty() ||
						   textField_3.getText().isEmpty()){
							JOptionPane.showMessageDialog(null, "Enter data in all fields");
						}
							
						else if(!DataCheckClass.nameCheck(textField.getText())){
							JOptionPane.showMessageDialog(null, "Enter valid color");
						}
						else if(!DataCheckClass.isDouble(textField_2.getText())){
							JOptionPane.showMessageDialog(null, "Enter valid price");
						}
						else if(!DataCheckClass.isInteger(textField_3.getText())){
							JOptionPane.showMessageDialog(null, "Enter valid number of objects");
						}
						else{
						RTV newObject = new RTV(seller.getName(), comboBox_1.getSelectedItem().toString(), Double.parseDouble(textField_2.getText()), 
								comboBox.getSelectedItem().toString(), comboBox_2.getSelectedItem().toString(), textField.getText(), 
								checkBox.isSelected(), Integer.parseInt(textField_3.getText()));
						seller.rtv.add(newObject);
						ProductSet.insertRTV(newObject);
						
						
						String query = "insert into RTV (seller_id, brand, product_name, energetic_class, has_guarantee, color," +
						"number_of_items, opis, price, model)" +
						" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
						
						PreparedStatement statement = connection.prepareStatement(query);
						statement.setString(1, Integer.toString(seller_id));
						statement.setString(2, comboBox_1.getSelectedItem().toString());
						statement.setString(3, comboBox.getSelectedItem().toString());
						statement.setString(4, comboBox_2.getSelectedItem().toString());
						statement.setString(5, Boolean.toString(checkBox.isSelected()));
						statement.setString(6, textField.getText());
						statement.setString(7, textField_3.getText());
						statement.setString(8, newObject.toString());
						statement.setString(9, textField_2.getText());
						statement.setString(10, "model");
						statement.execute();
						statement.close();
						
						JOptionPane.showMessageDialog(null, "New object was added");
						
						}
					}catch(Exception e1){
						JOptionPane.showMessageDialog(null, e1);
						
					}
				}
			});
		connection = SqlConnector.dbConnector();
	}

}
