package panels;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.AGD;
import model.DataCheckClass;
import model.ProductSet;
import model.Seller;
import model.SqlConnector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

public class AGDAddPanel extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5263259518350359785L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	Connection connection = null;
	private Seller seller;
	private int seller_id;
	public void setSeller( int id){
		try{
			connection = SqlConnector.dbConnector();
			String query = "select * from sellers where id = " +id;
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs= pst.executeQuery();
			while(rs.next()){
				seller = new Seller(rs.getString("name"), rs.getString("surname"));
			}
			rs.close();
			pst.close();
		}catch(Exception e){}
		seller_id = id;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AGDAddPanel frame = new AGDAddPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AGDAddPanel() {
		
		setBounds(100, 100, 328, 388);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddAgd = new JLabel("Add AGD");
		lblAddAgd.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddAgd.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddAgd.setBounds(10, 11, 300, 23);
		contentPane.add(lblAddAgd);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblType.setBounds(10, 45, 46, 14);
		contentPane.add(lblType);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Dryer", "Fridge", "Washing machine"}));
		comboBox.setBounds(118, 45, 153, 22);
		contentPane.add(comboBox);
		
		JLabel lblBrand = new JLabel("Brand");
		lblBrand.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBrand.setBounds(10, 70, 46, 14);
		contentPane.add(lblBrand);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"Beko", "Braun", "Candy", "Daewoo", "Electrolux", "Indesit", "Kenwood", "LG", "Moulinex", "Panasonic", "Philips", "Bosh", "Samsung", "Sony", "Tefal", "Zelmer"}));
		comboBox_1.setBounds(118, 70, 153, 22);
		contentPane.add(comboBox_1);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblColor.setBounds(10, 95, 46, 14);
		contentPane.add(lblColor);
		
		textField = new JTextField();
		textField.setBounds(118, 95, 153, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblModelName = new JLabel("Model name");
		lblModelName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblModelName.setBounds(10, 120, 86, 14);
		contentPane.add(lblModelName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(118, 120, 153, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblDepth = new JLabel("Depth");
		lblDepth.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDepth.setBounds(10, 145, 46, 14);
		contentPane.add(lblDepth);
		
		textField_2 = new JTextField();
		textField_2.setBounds(118, 145, 153, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblHeight = new JLabel("Height");
		lblHeight.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblHeight.setBounds(10, 170, 86, 14);
		contentPane.add(lblHeight);
		
		textField_3 = new JTextField();
		textField_3.setBounds(118, 170, 153, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblWidth = new JLabel("Width");
		lblWidth.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblWidth.setBounds(10, 195, 46, 14);
		contentPane.add(lblWidth);
		
		textField_4 = new JTextField();
		textField_4.setBounds(118, 195, 153, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblEnergeticClass = new JLabel("Energetic class");
		lblEnergeticClass.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnergeticClass.setBounds(10, 220, 86, 14);
		contentPane.add(lblEnergeticClass);
		
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.setModel(new DefaultComboBoxModel<String>(new String[] {"A+", "A", "B", "C", "D", "E", "F", "G"}));
		comboBox_2.setBounds(118, 220, 153, 22);
		contentPane.add(comboBox_2);
		
		JLabel lblWith = new JLabel("With guarantee");
		lblWith.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblWith.setBounds(10, 245, 96, 14);
		contentPane.add(lblWith);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.setBounds(118, 245, 97, 23);
		contentPane.add(checkBox);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPrice.setBounds(10, 270, 46, 14);
		contentPane.add(lblPrice);
		
		textField_5 = new JTextField();
		textField_5.setBounds(118, 270, 153, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNumberOfObjects = new JLabel("Number of objects");
		lblNumberOfObjects.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNumberOfObjects.setBounds(10, 295, 114, 14);
		contentPane.add(lblNumberOfObjects);
		
		textField_6 = new JTextField();
		textField_6.setBounds(118, 295, 153, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAdd.setBounds(124, 327, 91, 23);
		contentPane.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try{
						if(textField.getText().isEmpty() ||
						   textField_1.getText().isEmpty()	||
						   textField_2.getText().isEmpty() ||
						   textField_3.getText().isEmpty() ||
						   textField_4.getText().isEmpty() ||
						   textField_5.getText().isEmpty() ||
						   textField_6.getText().isEmpty()){
							JOptionPane.showMessageDialog(null, "Enter data in all fields");
						}
							
						else if(!DataCheckClass.nameCheck(textField.getText())){
							JOptionPane.showMessageDialog(null, "Enter valid color");
						}
						else if(!DataCheckClass.isDouble(textField_2.getText()) ){
							JOptionPane.showMessageDialog(null, "Enter valid depth");
						}
						else if(!DataCheckClass.isDouble(textField_3.getText()) ){
							JOptionPane.showMessageDialog(null, "Enter valid height");
						}
						else if(!DataCheckClass.isDouble(textField_4.getText()) ){
							JOptionPane.showMessageDialog(null, "Enter valid width");
						}
						else if(!DataCheckClass.isDouble(textField_5.getText()) ){
							JOptionPane.showMessageDialog(null, "Enter valid price");
						}
						else if(!DataCheckClass.isInteger(textField_6.getText())){
							JOptionPane.showMessageDialog(null, "Enter valid number of objects");
						}
						else{
						AGD agdObject = new AGD(seller, comboBox_1.getSelectedItem().toString(), Double.parseDouble(textField_5.getText()), comboBox.getSelectedItem().toString(), 
								Double.parseDouble(textField_2.getText()), Double.parseDouble(textField_3.getText()), Double.parseDouble(textField_4.getText()), 
								comboBox_2.getSelectedItem().toString(), textField.getText(), checkBox.isSelected(), Integer.parseInt(textField_6.getText()),  textField_1.getText());
						seller.agd.add(agdObject);
						ProductSet.insertAGD(agdObject);
						
						
						String query = "insert into AGD (owner_id, brand, price, product_name, color, depth, height, width, energetic_class, has_guarantee," +
								"number_of_items, model, opis)" +
								" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
						PreparedStatement statement = connection.prepareStatement(query);
						statement.setString(1, Integer.toString(seller_id));
						statement.setString(2, comboBox_1.getSelectedItem().toString());
						statement.setString(3, textField_5.getText()); // price
						statement.setString(4, comboBox.getSelectedItem().toString()); // product_name
						statement.setString(5, textField.getText());// color
						statement.setString(6, textField_2.getText()); // depth
						statement.setString(7, textField_3.getText());
						statement.setString(8, textField_4.getText());
						statement.setString(9, comboBox_2.getSelectedItem().toString()); // energetic_class
						statement.setString(10, Boolean.toString(checkBox.isSelected()));
						statement.setString(11, textField_6.getText());
						statement.setString(12, textField_1.getText());
						statement.setString(13, agdObject.toString());
						statement.execute();
						statement.close();
						
						JOptionPane.showMessageDialog(null, "New object was added");
						}
					}catch(Exception e1){
						JOptionPane.showMessageDialog(null, e1);
						
					}
				}
			});
		connection = SqlConnector.dbConnector();
	}
}
