package panels;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.SqlConnector;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UsersSelectedItems extends JFrame {

	/**
	 * 
	 */
	JList list;
	String opis = "";
	String table = "";
	String mainQuery = "";
	private static final long serialVersionUID = -6407688489698477842L;
	private JPanel contentPane;
	private JButton btnBuy;
	Map<String, Integer> map = new HashMap<String, Integer>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsersSelectedItems frame = new UsersSelectedItems();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static String convertToMultiline(String orig)
	{
	    return "<html>" + orig.replaceAll("\n", "<br>");
	}
	public static String convertToLine(String orig)
	{
		orig = orig.replaceAll("<br>", "\n");
		orig = orig.replaceAll("<html>", "");
	    return orig;
	}
	
	public void loadList(String query){
		try{
			
			Connection connection = SqlConnector.dbConnector();
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs= pst.executeQuery();
			
			DefaultListModel DLM = new DefaultListModel();
			
			while(rs.next()){
				DLM.addElement(convertToMultiline(rs.getString("opis")));
				map.put(convertToMultiline(rs.getString("opis")), rs.getInt("id"));
			}
			rs.close();
			pst.close();
			list.setModel(DLM);
		}catch(Exception e){}
	}
	
	void changeData(String mainType, String brand, String color, String itemType, 
			String model, double minPrice, double maxPrice, boolean withGuarantee){
		this.table = mainType;
		String query="";
		if(mainType=="AGD"){
			query = "select * from " +mainType + " where number_of_items >0" ;
		}
		else{
			query = "select * from " +mainType + " where number_of_items >0" ;
		}
		if(brand!="" || color!="" || itemType !="" || model !="" || minPrice!=0 || maxPrice!=-1 || withGuarantee){
			
			if(brand!=""){
				
				query += " and brand="+"\"" + brand+"\"";
			}
			if(color!=""){
				
					query += " and color = " + "\"" + color + "\"";
				
			}
			if(itemType!= ""){
				
					query += " and product_name = " + "\""+ itemType + "\"";
				
			}
			if(model != ""){
				query += " and model = " + "\""+ model + "\"";
					
			}
			if(maxPrice >0){
				
				query += " and price < " + maxPrice;
					
			}
			if(minPrice > 0){
				query += " and price > " + minPrice;
			}
			if(withGuarantee){
				query += " and has_guarantee = " + "\""+ withGuarantee + "\"";
			}
		}
		mainQuery = query;
		
		loadList(query);
		
	}

	/**
	 * Create the frame.
	 */
	public UsersSelectedItems() {
		
		setBounds(100, 100, 366, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 338, 251);
		contentPane.add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		
		btnBuy = new JButton("Buy");
		btnBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(list.getSelectedIndex()==-1){
					
				}else{
					try{
						opis = list.getSelectedValue().toString();
						String query = "select * from "+ table + " where id = " +map.get(opis);
						int number_of_items=1;
						int owner_id=-1;
						double additional_profit = 0;
						Connection connection = SqlConnector.dbConnector();
						PreparedStatement pst = connection.prepareStatement(query);
						ResultSet rs = pst.executeQuery();
						while(rs.next()){
							number_of_items = rs.getInt("number_of_items");
							
							if(table == "AGD"){owner_id = rs.getInt("owner_id");}
							else{ owner_id = rs.getInt("seller_id");}
							
							additional_profit = rs.getDouble("price");
							
							if(rs.getInt("number_of_items")==1){
								list.remove(list.getSelectedIndex());
								
							}
						}
						number_of_items--;
						query = "update "+ table + " set number_of_items = " + number_of_items +" where id = " +map.get(opis);
						System.out.println(query);
						connection = SqlConnector.dbConnector();
						pst = connection.prepareStatement(query);
						pst.execute();
						
						pst = connection.prepareStatement(mainQuery);
						rs= pst.executeQuery();
						
						DefaultListModel DLM = new DefaultListModel();
						
						while(rs.next()){
							DLM.addElement(convertToMultiline(rs.getString("opis")));
						}
						
						list.setModel(DLM);
						double profit = 0;
						query = "select profit from profit where id = " + owner_id;
						
						pst = connection.prepareStatement(query);
						rs= pst.executeQuery();
						
						while(rs.next()){
							profit = rs.getDouble("profit");
						}
						profit += additional_profit;
						query = "update profit set profit = " + profit +" where id = " + owner_id;
						
						pst = connection.prepareStatement(query);
						pst.execute();
						
						rs.close();
						pst.close();
						JOptionPane.showMessageDialog(null, "You have bought a new item");
					}catch(Exception e){
						JOptionPane.showMessageDialog(null, e);
					}
				}
			}
		});
		btnBuy.setBounds(123, 290, 91, 23);
		contentPane.add(btnBuy);
		
	}
}
