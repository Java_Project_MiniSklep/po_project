package panels;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import model.SqlConnector;

public class SellerSoldObjects extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	JLabel lblData;
	int seller_id=-1;
	Connection connection = null;
	
	void setSeller(int id){
		
		seller_id = id;
		changeData();
	}
	public static String convertToMultiline(String orig)
	{
	    return "<html>" + orig.replaceAll("\n", "<br>");
	}
	
	void changeData(){
		if(seller_id!=-1){
			String s="";
			
			try {
				connection = SqlConnector.dbConnector();
				
				String query = "select * from AGD where number_of_items = 0 and owner_id = "+Integer.toString(seller_id);
				String query1 = "select * from RTV where number_of_items = 0 and seller_id = "+Integer.toString(seller_id);
				
				PreparedStatement pst = connection.prepareStatement(query);
				ResultSet rs= pst.executeQuery();
				while(rs.next()){
					s+=rs.getString("opis")+'\n';
				}
				pst = connection.prepareStatement(query1);
				rs= pst.executeQuery();
				while(rs.next()){
					s+=rs.getString("opis")+'\n';
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			lblData.setText(convertToMultiline(s));
		}
		
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SellerSoldObjects frame = new SellerSoldObjects();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SellerSoldObjects() {
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 422, 251);
		contentPane.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.WHITE);
		panel.setToolTipText("My products");
		scrollPane.setColumnHeaderView(panel);
		
		JLabel lblMyProducts = new JLabel("Sold products");
		lblMyProducts.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblMyProducts);
		
		
		String s="";
		
		lblData = new JLabel(s);
		lblData.setBackground(SystemColor.inactiveCaptionBorder);
		lblData.setVerticalAlignment(SwingConstants.TOP);
		scrollPane.setViewportView(lblData);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setRowHeaderView(scrollPane_1);
	}

}
