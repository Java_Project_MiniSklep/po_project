package panels;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Client;
import model.ClientSet;
import model.DataCheckClass;
import model.Seller;
import model.SellerSet;
import model.SqlConnector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class SignUp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6775291805261677830L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JPasswordField passwordField;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignUp frame = new SignUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SignUp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblLogin.setBounds(20, 10, 77, 30);
		contentPane.add(lblLogin);
		
		textField = new JTextField();
		textField.setBounds(107, 17, 110, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPassword.setBounds(20, 40, 67, 30);
		contentPane.add(lblPassword);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblName.setBounds(20, 70, 77, 30);
		contentPane.add(lblName);
		
		JLabel lblSurname = new JLabel("Surname");
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSurname.setBounds(20, 100, 77, 30);
		contentPane.add(lblSurname);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEmail.setBounds(20, 130, 77, 30);
		contentPane.add(lblEmail);
		
		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPhone.setBounds(20, 160, 67, 30);
		contentPane.add(lblPhone);
		
		JLabel lblSignUpAs = new JLabel("Sign up as");
		lblSignUpAs.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSignUpAs.setBounds(20, 190, 67, 30);
		contentPane.add(lblSignUpAs);
		
		textField_2 = new JTextField();
		textField_2.setBounds(107, 76, 110, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(107, 106, 110, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(107, 136, 110, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(107, 166, 110, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		ButtonGroup gr = new ButtonGroup();
		
		JRadioButton rdbtnUser = new JRadioButton("User");
		rdbtnUser.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnUser.setBounds(107, 194, 67, 23);
		contentPane.add(rdbtnUser);
		gr.add(rdbtnUser);
		JRadioButton rdbtnSeller = new JRadioButton("Seller");
		rdbtnSeller.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnSeller.setBounds(176, 194, 109, 23);
		contentPane.add(rdbtnSeller);
		gr.add(rdbtnSeller);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(107, 48, 110, 20);
		contentPane.add(passwordField);
		
		JButton btnSignUp = new JButton("Sign up");
		btnSignUp.setBounds(20, 232, 412, 30);
		contentPane.add(btnSignUp);
		btnSignUp.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try{
					if((!rdbtnSeller.isSelected() && !rdbtnUser.isSelected())
							|| textField.getText().isEmpty()
							||textField_2.getText().isEmpty()
							||textField_3.getText().isEmpty()
							||textField_4.getText().isEmpty()
							||textField_5.getText().isEmpty()
							|| passwordField.getPassword().length ==0 ){
						JOptionPane.showMessageDialog(null, "Enter data in all fields");
					}
					else if(!DataCheckClass.e_mailCheck(textField_4.getText())){
						JOptionPane.showMessageDialog(null, "Enter valid e-mail");
					}
					else if(!DataCheckClass.loginCheck(textField.getText())){
						JOptionPane.showMessageDialog(null, "Enter valid login");
					}
					else if(!DataCheckClass.passwordCheck(new String(passwordField.getPassword()))){
						JOptionPane.showMessageDialog(null, "Your password is too short");
					}
					else if(!DataCheckClass.nameCheck(textField_2.getText())){
						JOptionPane.showMessageDialog(null, "Enter valid name");
					}
					else if(!DataCheckClass.nameCheck(textField_3.getText())){
						JOptionPane.showMessageDialog(null, "Enter valid surname");
					}
					else if(!DataCheckClass.phoneCheck(textField_5.getText())){
						JOptionPane.showMessageDialog(null, "Enter valid phone");
					}
					else{
						
						
						String query = null;
						String query1 = null;
						if(rdbtnSeller.isSelected()){
							query = "select * from Sellers where login=?";
							query1 = "insert into Sellers (login, password, name, surname, e_mail, phone) values (?, ?, ?, ?, ?, ?)";
							
						}
						else{
							query = "select * from Users where login=?";
							query1 = "insert into Users (login, password, name, surname, e_mail, phone) values (?, ?, ?, ?, ?, ?)";
						}
						PreparedStatement pst = connection.prepareStatement(query);
						pst.setString(1, textField.getText());
						ResultSet rs= pst.executeQuery();
						int count = 0;
						while(rs.next()){
							count++;
						}
						if(count>0){
							JOptionPane.showMessageDialog(null, "This login is already used");
						}
						else{
							PreparedStatement statement = connection.prepareStatement(query1);
							statement.setString(1, textField.getText());
							statement.setString(2, new String(passwordField.getPassword()));
							statement.setString(3, new String(textField_2.getText()));
							statement.setString(4, new String(textField_3.getText()));
							statement.setString(5, new String(textField_4.getText()));
							statement.setString(6, new String(textField_5.getText()));
							statement.execute();
							statement.close();
							
							pst = connection.prepareStatement(query);
							pst.setString(1, textField.getText());
							rs= pst.executeQuery();
							count = 0;
							int id=-1;
							while(rs.next()){
								id = rs.getInt ("id");
							}
							
							if(rdbtnSeller.isSelected()){
								
								SellerSet.insert(id, new Seller(new String(textField_2.getText()), new String(textField_3.getText())));
								query = "insert into profit (id, profit) values ( "+id+", 0 )";
								pst = connection.prepareStatement(query);
								pst.execute();
								
								
								
							}
							else{
								ClientSet.insert(id, new Client(new String(textField_2.getText()), new String(textField_3.getText())));
							}
							
							rs.close();
							pst.close();
							
							java.awt.Window win[] = java.awt.Window.getWindows(); 
							for(int i=0;i<win.length;i++){ 
								win[i].dispose(); 
							}
							
							if(rdbtnSeller.isSelected()){
								
								SellerPanel sellerPanel = new SellerPanel();
								sellerPanel.setid(id);
								sellerPanel.setVisible(true);
								
							}
							else{
								UserPanel userPanel = new UserPanel();
								userPanel.set_id(id);
								userPanel.setVisible(true);
							}
							
						}
					}
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, e1);
				}
			}
		});
		
		connection = SqlConnector.dbConnector();
	}
}
