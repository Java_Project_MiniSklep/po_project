package panels;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import model.DataCheckClass;

public class UserPanel extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4108863816376576748L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private int user_id =-1; 
	void set_id(int id ){
		user_id = id;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserPanel frame = new UserPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserPanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 341, 344);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSelectNewItem = new JLabel("Select new item");
		lblSelectNewItem.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectNewItem.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSelectNewItem.setBounds(10, 11, 313, 14);
		contentPane.add(lblSelectNewItem);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblType.setBounds(10, 35, 63, 25);
		contentPane.add(lblType);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(136, 63, 110, 22);
		contentPane.add(comboBox_1);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"AGD", "RTV"}));
		comboBox.setBounds(136, 36, 110, 22);
		contentPane.add(comboBox);
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBox.getSelectedItem()=="AGD"){
					comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"Fridge", "Washing Mashine", "Dryers"}));
				}
				else{
					comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"TV", "Earphones", "Home cinema"}));
				}
			}
		});
		
		JLabel lblItemName = new JLabel("Item name");
		lblItemName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblItemName.setBounds(10, 60, 91, 25);
		contentPane.add(lblItemName);
		
		JLabel lblBrand = new JLabel("Brand");
		lblBrand.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblBrand.setBounds(10, 85, 110, 25);
		contentPane.add(lblBrand);
		
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.setModel(new DefaultComboBoxModel<String>(new String[] {"Panasonic", "Sony", "Lenovo", "Philips", "LG", "Bosh"}));
		comboBox_2.setBounds(136, 88, 110, 22);
		contentPane.add(comboBox_2);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColor.setBounds(10, 110, 110, 25);
		contentPane.add(lblColor);
		
		JComboBox<String> comboBox_3 = new JComboBox<String>();
		comboBox_3.setModel(new DefaultComboBoxModel<String>(new String[] {"White", "Black", "Grey", "Red", "Other"}));
		comboBox_3.setBounds(136, 113, 110, 22);
		contentPane.add(comboBox_3);
		
		JLabel lblWithGa = new JLabel("With guarantee");
		lblWithGa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblWithGa.setBounds(10, 210, 110, 25);
		contentPane.add(lblWithGa);
		
		JRadioButton radioButton = new JRadioButton("");
		radioButton.setBounds(136, 212, 109, 23);
		contentPane.add(radioButton);
		
		JLabel lblModel = new JLabel("Model");
		lblModel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModel.setBounds(10, 135, 46, 25);
		contentPane.add(lblModel);
		
		textField = new JTextField();
		textField.setBounds(136, 139, 110, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPrice = new JLabel("Min Price");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPrice.setBounds(10, 160, 91, 25);
		contentPane.add(lblPrice);
		
		JLabel lblMaxPrice = new JLabel("Max Price");
		lblMaxPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaxPrice.setBounds(10, 185, 91, 25);
		contentPane.add(lblMaxPrice);
		
		textField_1 = new JTextField();
		textField_1.setBounds(136, 164, 110, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(136, 189, 110, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		JButton btnNewButton;
		if(user_id!=-1){
			btnNewButton = new JButton("Search");
			btnNewButton.setBounds(10, 246, 148, 36);
			contentPane.add(btnNewButton);
		
			JButton btnNewButton_1 = new JButton("Selected items");
			btnNewButton_1.setBounds(178, 246, 145, 36);
			contentPane.add(btnNewButton_1);
		}
		else{
			btnNewButton = new JButton("Search");
			btnNewButton.setBounds(82, 246, 148, 36);
			contentPane.add(btnNewButton);
			
		}
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBox.getSelectedIndex() != -1){
					String mainType = "";
					mainType = (String) comboBox.getSelectedItem();
					String brand = "";
					String color = "";
					String itemType = "";
					String model = "";
					double minPrice = 0;
					double maxPrice = -1;
					boolean withGuarantee=false;
					if(comboBox_2.getSelectedIndex()!=-1){
						brand = (String) comboBox_2.getSelectedItem();
					}
					if(comboBox_3.getSelectedIndex()!=-1){
						color = (String) comboBox_3.getSelectedItem();
					}
					if(radioButton.isSelected()){
						withGuarantee = true;
					}
					if(comboBox_1.getSelectedIndex()!=-1){
						itemType = (String) comboBox_2.getSelectedItem();
					}
					if(!textField.getText().isEmpty()){
						model = textField.getText();
					}
					if(!textField_1.getText().isEmpty()){
						if(DataCheckClass.isDouble(textField_1.getText())){
							minPrice = Double.parseDouble(textField_1.getText()) ;
						}
						else{
							JOptionPane.showMessageDialog(null, "Enter valid min price");
						}
					}
					if(!textField_1.getText().isEmpty()){
						if(DataCheckClass.isDouble(textField_2.getText())){
							maxPrice = Double.parseDouble(textField_1.getText()) ;
						}
						else{
							JOptionPane.showMessageDialog(null, "Enter valid max price");
						}
					}
					
					
				}
				
			}
		});
		
	}
}
